package br.com.ziben.jms;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.JMSRuntimeException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

//@RequestScoped
public class JMSService {
 
    //@Resource(name = "java:/myJmsTest/MyConnectionFactory")
    private ConnectionFactory connectionFactory;

    //@Resource(name = "java:/myJmsTest/MyQueue")
    Destination destination;
 
    public JMSService() throws NamingException {
    	  this.connectionFactory = InitialContext.doLookup("myJmsTest/MyConnectionFactory");
    	  this.destination = InitialContext.doLookup("myJmsTest/MyQueue");
    }
    
    public void sendMessage(String txt) {
 
        try {
        	System.out.println("**** Entrou pra enviar...");

        	try {
        		JMSContext jcontext = connectionFactory.createContext("myJmsUser", "myJmsPassword");
        		   JMSProducer mp = jcontext.createProducer();
        		   mp.send(destination, txt);
        		  } catch (JMSRuntimeException re) {
        		   re.printStackTrace();
        	}
/*
            //Authentication info can be omitted if we are using in-vm
            QueueConnection connection = (QueueConnection) connectionFactory.createConnection("myJmsUser", "myJmsPassword");

            try {
                QueueSession session = connection.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);

                try {
                    MessageProducer producer = session.createProducer(destination);

                    try {
                        TextMessage message = session.createTextMessage("Hello, world! ^__^" );

                        producer.send(message);

                        System.out.println("Message sent! ^__^");
                    } finally {
                        producer.close();
                    }
                } finally {
                    session.close();
                }

            } finally {
                connection.close();
            }
*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}